package mbds.av.fr.td_messagerie;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Base64;
import android.util.Log;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import javax.crypto.SecretKey;

import mbds.av.fr.td_messagerie.Utils.Crypto;
import mbds.av.fr.td_messagerie.Utils.EncryptedDataAES;
import mbds.av.fr.td_messagerie.database.Database;
import mbds.av.fr.td_messagerie.enums.ETypeMessage;
import mbds.av.fr.td_messagerie.model.ContactModel;
import mbds.av.fr.td_messagerie.model.MessageModel;
import mbds.av.fr.td_messagerie.task.OnRetrieveListTask;
import mbds.av.fr.td_messagerie.task.RetrieveMessageTask;

public class MessageService extends Service {
    private Handler mHandler;
    // default interval for syncing data
    private static long nbSecond = 5;
    private Context context;
    public static final String ANDROID_CHANNEL_NAME = "MESSAGE CHANNEL";
    private final static AtomicInteger c = new AtomicInteger(0);

    public MessageService(){
        this.context = this;
    }

    // task to be run here
    private Runnable runnableService = new Runnable() {
        @Override
        public void run() {
            // Check if user is connected
            SharedPreferences pref = context.getSharedPreferences("api", Context.MODE_PRIVATE);
            String actualUser = pref.getString("actualUser",null);
            try {
                KeyStore keyStore = Crypto.getKeyStore();
                if(actualUser != null) {
                    System.out.println("ActualUser: "+ actualUser);
                    Database db = Database.getInstance(context);
                    ArrayList<ContactModel> listContact = db.readContacts(actualUser);
                    // Get all messages for actual user's contact
                    for (ContactModel contact : listContact) {
                        System.out.println("Contact " + contact.getUsername());
                        RetrieveMessageTask task = new RetrieveMessageTask(context);
                        task.setOnRetrieveContactTask(new OnRetrieveListTask<MessageModel>() {
                            @Override
                            public void onTaskCompleted(final ArrayList<MessageModel> listMessage) {
                                ArrayList<MessageModel> newMessages = new ArrayList<>();
                                for (MessageModel message : listMessage) {
                                    if (!message.isAlreadyReturned()) {
                                        db.addMessage(message, actualUser);
                                        newMessages.add(message);
                                    }
                                }

                                if (newMessages.isEmpty()) {
                                    nbSecond += 5;
                                } else {
                                    // Notify to refresh list message
                                    Intent intent = new Intent("REFRESH_LIST_MESSAGE");
                                    sendBroadcast(intent);

                                    SecretKey secretKey = null;
                                    try {
                                        if (keyStore.containsAlias(actualUser + "_to_" + contact.getUsername())) {
                                            secretKey = ((KeyStore.SecretKeyEntry) keyStore.getEntry(actualUser + "_to_" + contact.getUsername(), null)).getSecretKey();
                                        }
                                        // Create notification
                                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context
                                                , ANDROID_CHANNEL_NAME)
                                                .setSmallIcon(R.drawable.ic_message)
                                                .setContentTitle(contact.getFullName())
                                                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                                        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

                                        // Decode messages
                                        for (MessageModel message : newMessages) {
                                            mBuilder.setContentText(message.getDecodedMessage(secretKey, false));
                                            notificationManager.notify(getID(), mBuilder.build());
                                        }

                                    } catch (Exception ex) {
                                        System.out.println("ERROR: " + ex.getMessage());
                                    }
                                }

                                if (nbSecond > 30) {
                                    nbSecond = 5;
                                }
                                System.out.println(nbSecond);
                                System.out.println(nbSecond * 1000);
                                mHandler.postDelayed(runnableService, nbSecond * 1000);
                            }

                            @Override
                            public void onFailure(String message) {
                                Log.d("DEBUG API", message);
                            }
                        });
                        task.execute();
                    }
                    if(listContact.isEmpty()){
                        nbSecond += 5;
                        if (nbSecond > 30) {
                            nbSecond = 5;
                        }
                        mHandler.postDelayed(runnableService, nbSecond * 1000);
                    }
                }else{
                    System.out.println("User not connected");
                    mHandler.postDelayed(runnableService, nbSecond * 1000);
                }
            }catch(Exception ex){
                System.out.println("ERROR: " + ex.getMessage());
            }

        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Create the Handler object
        mHandler = new Handler();
        // Execute a runnable task as soon as possible
        mHandler.post(runnableService);
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static int getID() {
        return c.incrementAndGet();
    }
}

