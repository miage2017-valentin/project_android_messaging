package mbds.av.fr.td_messagerie;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import mbds.av.fr.td_messagerie.fragment.ListContactFragment;
import mbds.av.fr.td_messagerie.fragment.ListMessageFragment;
import mbds.av.fr.td_messagerie.model.ContactModel;

public class HomePageActivity extends AppCompatActivity implements ICallable {

    private ListContactFragment listContactFragment = new ListContactFragment();
    private ListMessageFragment listMessageFragment = new ListMessageFragment();
    private boolean isSingleViewed;
    private String sharedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();

        FrameLayout fm = findViewById(R.id.listMessage);

        // Checks the orientation of the screen
        if (fm == null) {
            ft.replace(R.id.listContact, this.listContactFragment);
        } else {
            ft.replace(R.id.listContact, this.listContactFragment);
            ft.replace(R.id.listMessage, this.listMessageFragment);
        }
        isSingleViewed = (fm == null);
        ft.commit();

        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method to change contact in listMessageFragment
     * @param contactModel
     */
    @Override
    public void changeContact(ContactModel contactModel) {
        SharedPreferences sharedPreferences = this.getSharedPreferences("contact", Context.MODE_PRIVATE);
        sharedPreferences.edit().putLong("chooseContact", contactModel.getId()).commit();
        if (sharedText != null) {
            this.listMessageFragment.setContentMessage(sharedText);
            this.sharedText = "";
        }
        listMessageFragment.setContact(contactModel);
        if (isSingleViewed) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.listContact, listMessageFragment);
            fragmentTransaction.commit();
        }
    }

    /**
     * Method to return display ListContactFragment
     */
    @Override
    public void returnToListContact() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.listContact, listContactFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void openActivity(Class activity) {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
    }

    /**
     * Remove token and actualUser variable in shared preferences and finish activity
     */
    @Override
    public void logout() {
        SharedPreferences sharedPreferences = this.getSharedPreferences("api", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("token");
        editor.remove("actualUser");
        editor.apply();
        finish();
    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            Snackbar.make(findViewById(R.id.listContact), sharedText, Snackbar.LENGTH_SHORT)
                    .show();
        }
        this.sharedText = sharedText;
    }
}
