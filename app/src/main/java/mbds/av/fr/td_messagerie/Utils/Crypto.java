package mbds.av.fr.td_messagerie.Utils;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.security.keystore.KeyProtection;
import android.util.Base64;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {
    private static String provider = "AndroidKeyStore";
    private static String transformation = "RSA/ECB/PKCS1Padding";
    private static String transformationAES = "AES/GCM/NoPadding";

    public static KeyPair generateKeyPair(String keyStoreAlias) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", provider);
        KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(keyStoreAlias, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_ECB)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1);
        kpg.initialize(builder.build());
        return kpg.genKeyPair();
    }

    public static SecretKey generateKey() throws NoSuchAlgorithmException {
        return KeyGenerator.getInstance("AES").generateKey();
    }

    public static KeyStore getKeyStore() throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException {
            KeyStore keyStore = KeyStore.getInstance(provider);
            keyStore.load(null);
            return keyStore;
    }

    public static byte[] decrypt(PrivateKey privateKey, byte[] encryptedBytes) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher1 = Cipher.getInstance(transformation);
        cipher1.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = cipher1.doFinal(encryptedBytes);
        return decryptedBytes;
    }

    public static byte[] encrypt(PublicKey publicKey, byte[] data) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher.doFinal(data);
    }

    public static PublicKey stringToPublicKey(String strPublicKey) throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {
        byte[] publicBytes = Base64.decode(strPublicKey,0);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(keySpec);
    }

    public static SecretKey stringEncryptedToSecretKey(String strSecretKey, PrivateKey privateKey) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        byte[] publicBytes = Base64.decode(strSecretKey,0);
        byte[] decryptedBytes = Crypto.decrypt(privateKey, publicBytes);
        return new SecretKeySpec(decryptedBytes, 0, decryptedBytes.length, "AES");
    }

    public static EncryptedDataAES encryptAES(SecretKey key, byte[] data) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher cipher = Cipher.getInstance(transformationAES);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return new EncryptedDataAES(cipher.doFinal(data), cipher.getIV());
    }

    public static byte[] decryptAES(SecretKey key, EncryptedDataAES encryptedDataAES) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        Cipher cipher = Cipher.getInstance(transformationAES);
        GCMParameterSpec spec = new GCMParameterSpec(128, encryptedDataAES.getCipherIV());
        cipher.init(Cipher.DECRYPT_MODE, key, spec);
        return cipher.doFinal(encryptedDataAES.getData());
    }

    public static void storeSecretKey(SecretKey key, String keyStoreAlias ) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
        KeyStore keyStore = Crypto.getKeyStore();
        keyStore.setEntry(
                keyStoreAlias,
                new KeyStore.SecretKeyEntry(key),
                new KeyProtection.Builder(KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                        .build());
    }
}
