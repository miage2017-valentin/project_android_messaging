package mbds.av.fr.td_messagerie.task;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import mbds.av.fr.td_messagerie.model.MessageModel;

public class RetrieveMessageTask extends AsyncTask<Void, Void, JSONArray> {

    private OnRetrieveListTask listener;
    private SharedPreferences sharedPreferences;

    public RetrieveMessageTask(Context context) {
        this.sharedPreferences = context.getSharedPreferences("api", Context.MODE_PRIVATE);
    }

    public void setOnRetrieveContactTask(OnRetrieveListTask listener) {
        this.listener = listener;
    }

    protected void onPreExecute() {
    }

    protected JSONArray doInBackground(Void... urls) {
        System.out.println("---- SEND ----");
        try {
            URL url = new URL("http://baobab.tokidev.fr/api/fetchMessages");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty("Authorization", "Bearer " + this.sharedPreferences.getString("token", ""));
                System.out.println("Token : " + this.sharedPreferences.getString("token", "No token"));
                System.out.println("Code response : " + urlConnection.getResponseCode());
                InputStream responseBody = urlConnection.getInputStream();
                InputStreamReader responseBodyReader =
                        new InputStreamReader(responseBody, "UTF-8");
                BufferedReader streamReaderBuffer = new BufferedReader(responseBodyReader);
                String inputStr;
                StringBuilder responseStrBuilder = new StringBuilder();
                while ((inputStr = streamReaderBuffer.readLine()) != null)
                    responseStrBuilder.append(inputStr);

                JSONArray objResponse = new JSONArray(responseStrBuilder.toString());
                return objResponse;
            } catch (Exception ex) {
                System.out.println("ERROR : " + ex.getMessage());
                return null;
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e("ERROR OPEN CONNECTION : ", e.getMessage(), e);
            return null;
        }
    }

    protected void onPostExecute(JSONArray response) {
        if (response == null) {
            Log.d("DEBUG API", "ERROR response");
            listener.onFailure("Erreur API");
        } else {
            System.out.println("DEBUG API : " + response.toString());
            try {
                ArrayList<MessageModel> returnList = new ArrayList<>();
                for (int i = 0; i < response.length(); i++) {
                    JSONObject json = response.getJSONObject(i);
                    MessageModel message = new MessageModel(
                            json.getLong("id"),
                            json.getString("author"),
                            this.sharedPreferences.getString("actualUser", ""),
                            json.getString("msg"),
                            json.getBoolean("alreadyReturned")
                    );

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                    format.setTimeZone(TimeZone.getTimeZone("UTC"));
                    try {
                        Date dateCreated = format.parse(json.getString("dateCreated"));
                        message.setDateCreated(dateCreated);
                    } catch (ParseException e) {
                        System.out.println("ERROR PARSING : " + e.getMessage());
                    }
                    returnList.add(message);
                }
                listener.onTaskCompleted(returnList);
            } catch (JSONException e) {
                listener.onFailure("Erreur API");
            }
        }
    }
}
