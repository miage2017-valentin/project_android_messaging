package mbds.av.fr.td_messagerie;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import mbds.av.fr.td_messagerie.database.Database;

public class AddContactActivity extends AppCompatActivity {

    private EditText txtName;
    private EditText txtLastName;
    private EditText txtUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        this.txtUsername = findViewById(R.id.txtUsername);
        this.txtLastName = findViewById(R.id.txtLastName);
        this.txtName = findViewById(R.id.txtName);
        final Context context = this;


        SharedPreferences pref = this.getSharedPreferences("api", Context.MODE_PRIVATE);
        String actualUser = pref.getString("actualUser","");

        // Button to add contact in local database
        Button btnAddContact = findViewById(R.id.btnAddContact);
        btnAddContact.setOnClickListener(view -> {
            Database db = Database.getInstance(context);
            db.addContact(txtUsername.getText().toString(),
                    txtLastName.getText().toString(),
                    txtName.getText().toString(),
                    actualUser);
            finish();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

