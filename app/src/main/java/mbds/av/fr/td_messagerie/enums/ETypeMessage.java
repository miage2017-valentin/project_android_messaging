package mbds.av.fr.td_messagerie.enums;

public enum ETypeMessage {
    PING, PONG, MSG
}
