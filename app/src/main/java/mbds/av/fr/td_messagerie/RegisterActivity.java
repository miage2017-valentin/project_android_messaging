package mbds.av.fr.td_messagerie;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import mbds.av.fr.td_messagerie.task.OnTaskCompleted;
import mbds.av.fr.td_messagerie.task.RegisterUserTask;

public class RegisterActivity extends AppCompatActivity {
    private EditText txtUsername, txtPassword;
    private Button btnRegister;
    private ImageButton btnReturnToLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        btnRegister = findViewById(R.id.btnRegister);
        btnReturnToLogin = findViewById(R.id.btnReturnToLogin);

        btnReturnToLogin.setOnClickListener(v-> this.finish());

        // Register button
        btnRegister.setOnClickListener(view -> {
            final ProgressDialog dialog = ProgressDialog.show(RegisterActivity.this, "",
                    "Creating user...", true);
            dialog.show();
            String username = txtUsername.getText().toString();
            String password = txtPassword.getText().toString();
            RegisterUserTask task = new RegisterUserTask(username, password);
            task.setOnTaskCompleted(new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {
                    dialog.hide();
                    Snackbar.make(view, "User created with success", Snackbar.LENGTH_LONG)
                            .show();
                }

                @Override
                public void onFailure(String message) {
                    dialog.hide();
                    Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                            .show();
                }
            });
            task.execute();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
