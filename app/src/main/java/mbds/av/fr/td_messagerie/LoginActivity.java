package mbds.av.fr.td_messagerie;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.security.KeyStore;

import mbds.av.fr.td_messagerie.Utils.Crypto;
import mbds.av.fr.td_messagerie.task.LoginTask;
import mbds.av.fr.td_messagerie.task.OnTaskCompleted;


public class LoginActivity extends AppCompatActivity{

    private EditText loginBox;
    private EditText passwordBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent serviceIntent = new Intent(getApplicationContext(), MessageService.class);
        startService(serviceIntent);
        setContentView(R.layout.activity_login);

        this.loginBox = (EditText) findViewById(R.id.loginBox);
        this.passwordBox = (EditText) findViewById(R.id.passwordBox);

        final Context context = this;

        // If user is already connected, show list contact
        SharedPreferences sharedPreferences = context.getSharedPreferences("api", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token",null);
        if(token != null){
            this.showList(findViewById(R.id.btnConnect));
        }

        // Click on "login" button
        Button btnConnect = findViewById(R.id.btnConnect);
        btnConnect.setOnClickListener(view -> {
            final ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "",
                    "Loading. Please wait...", true);
            dialog.show();
            LoginTask task = new LoginTask(loginBox.getText().toString(),passwordBox.getText().toString(), context);
            task.setOnTaskCompleted(new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {
                    String login = loginBox.getText().toString();
                    dialog.hide();
                    // Generate keypair RSA after login if it's not exist
                    try {
                        KeyStore keyStore = Crypto.getKeyStore();
                        if (!keyStore.containsAlias(login)) {
                            Crypto.generateKeyPair(login);
                        }
                    } catch(Exception ex){
                        ex.printStackTrace();
                    }
                    showList(view);
                }

                @Override
                public void onFailure(String message) {
                    dialog.hide();
                    Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
                            .show();
                }
            });
            task.execute();
        });

        // Show register form
        Button btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(view -> showRegisterForm(view));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list_message, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showList(View view) {
        Intent intent = new Intent(this, HomePageActivity.class);
        startActivity(intent);
    }

    public void showRegisterForm(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }
}
