package mbds.av.fr.td_messagerie.Utils;

public class EncryptedDataAES {
    private byte[] data;
    private byte[] cipherIV;

    public EncryptedDataAES(byte[] data, byte[] cipherIV) {
        this.data = data;
        this.cipherIV = cipherIV;
    }

    public byte[] getData() {
        return data;
    }

    public byte[] getCipherIV() {
        return cipherIV;
    }
}
