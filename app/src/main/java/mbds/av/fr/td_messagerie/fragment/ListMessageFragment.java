package mbds.av.fr.td_messagerie.fragment;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.crypto.SecretKey;

import mbds.av.fr.td_messagerie.ICallable;
import mbds.av.fr.td_messagerie.R;
import mbds.av.fr.td_messagerie.Utils.Crypto;
import mbds.av.fr.td_messagerie.Utils.EncryptedDataAES;
import mbds.av.fr.td_messagerie.adapter.MessageAdapter;
import mbds.av.fr.td_messagerie.database.Database;
import mbds.av.fr.td_messagerie.enums.ETypeMessage;
import mbds.av.fr.td_messagerie.model.ContactModel;
import mbds.av.fr.td_messagerie.model.MessageModel;
import mbds.av.fr.td_messagerie.task.OnRetrieveListTask;
import mbds.av.fr.td_messagerie.task.OnTaskCompleted;
import mbds.av.fr.td_messagerie.task.RetrieveMessageTask;
import mbds.av.fr.td_messagerie.task.SendMessageTask;

public class ListMessageFragment extends Fragment {

    private Context mContext;
    private ICallable mCallback;
    private View view;
    private EditText boxMessage;
    private TextView txtName;
    private ImageButton btnReturn, btnSend;
    private Button btnActionCrypto;
    private ContactModel contact;
    private RecyclerView listView;
    private Database db;
    private String contentMessage;
    private String actualUser;
    private BroadcastReceiver broadcastReceiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_list_message, container, false);

        db = Database.getInstance(mContext);
        SharedPreferences pref = mContext.getSharedPreferences("api", Context.MODE_PRIVATE);
        actualUser = pref.getString("actualUser", "");
        SharedPreferences prefContact = mContext.getSharedPreferences("contact", Context.MODE_PRIVATE);

        if (prefContact.contains("chooseContact")) {
            this.contact = db.readContactById(actualUser, prefContact.getLong("chooseContact", new Long(-1)));
        }
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                initView();
            }
        };

        btnActionCrypto = this.view.findViewById(R.id.btnActionCrypto);
        boxMessage = this.view.findViewById(R.id.boxMessage);
        txtName = this.view.findViewById(R.id.txtName);
        btnSend = this.view.findViewById(R.id.btnSend);
        btnReturn = this.view.findViewById(R.id.btnReturn);
        btnReturn.setOnClickListener(v -> returnToListContact());

        // Hide return button on layout landscape
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            this.btnReturn.setVisibility(View.GONE);
        } else {
            this.btnReturn.setVisibility(View.VISIBLE);
        }

        listView = view.findViewById(R.id.listMessage);
        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        listView.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(listView.getContext(),
                mLayoutManager.getOrientation());
        listView.addItemDecoration(dividerItemDecoration);

        btnSend.setOnClickListener(v -> sendMessage());

        final ProgressDialog dialog = ProgressDialog.show(mContext, "",
                "Loading. Retrieve messages...", true);
        dialog.show();

        RetrieveMessageTask task = new RetrieveMessageTask(mContext);
        task.setOnRetrieveContactTask(new OnRetrieveListTask<MessageModel>() {
            @Override
            public void onTaskCompleted(final ArrayList<MessageModel> listMessage) {
                ArrayList<MessageModel> newMessages = new ArrayList<>();
                for (MessageModel message : listMessage) {
                    if (!message.isAlreadyReturned()) {
                        db.addMessage(message, actualUser);
                        newMessages.add(message);
                    }
                }
                dialog.hide();
                initView();
                IntentFilter intFilt = new IntentFilter("REFRESH_LIST_MESSAGE");
                mContext.registerReceiver(broadcastReceiver, intFilt);
            }

            @Override
            public void onFailure(String message) {
                Log.d("DEBUG API", message);
            }
        });
        task.execute();


        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        if (context instanceof ICallable) {
            mCallback = (ICallable) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement iCallable");
        }
    }

    public void setContact(ContactModel contact) {
        this.contact = contact;
        if (this.view != null) {
            initView();
        }
    }

    public void initView() {
        if (contact != null) {
            setVisibiltyElements(View.VISIBLE);
            txtName.setText(contact.getFullName());
            boxMessage.setText(this.contentMessage);
            refreshListMessage();
            try {
                KeyStore keyStore = Crypto.getKeyStore();

                enabledMessageBoxAndButtonSend();

                ArrayList<MessageModel> messages = ((MessageAdapter) listView.getAdapter()).getMessages();
                List<MessageModel> messagesPing = messages.stream().filter(message ->
                        message.getType() != null && message.getType().equals(ETypeMessage.PING)).collect(Collectors.toList());

                List<MessageModel> messagesPong = messages.stream().filter(message ->
                        message.getType() != null && message.getType().equals(ETypeMessage.PONG)).collect(Collectors.toList());

                if (!keyStore.containsAlias(this.actualUser + "_to_" + contact.getUsername())) {
                    if (messagesPing.isEmpty()) {
                        btnActionCrypto.setText("Ask key");
                        btnActionCrypto.setOnClickListener(v -> {
                            try {
                                PublicKey publicKey = keyStore.getCertificate(actualUser).getPublicKey();

                                byte[] publicKeyBytes = Base64.encode(publicKey.getEncoded(), 0);
                                final String message = this.actualUser + "[|]PING[|]" + new String(publicKeyBytes);
                                SendMessageTask task = new SendMessageTask(contact.getUsername(), message, mContext);
                                task.setOnTaskCompleted(new OnTaskCompleted() {
                                    @Override
                                    public void onTaskCompleted() {
                                        db.addMessage(new MessageModel(null, actualUser, contact.getUsername(), message, true), actualUser);
                                        refreshListMessage();
                                    }

                                    @Override
                                    public void onFailure(String message) {
                                        System.out.println(message);
                                    }
                                });
                                task.execute();


                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        });
                    } else {
                        if (messagesPong.isEmpty()) {
                            btnActionCrypto.setText("Send key");
                            btnActionCrypto.setOnClickListener(v -> {
                                try {
                                    MessageModel lastPing = messagesPing.get(messagesPing.size() - 1);
                                    PublicKey pk = Crypto.stringToPublicKey(lastPing.getMessage());
                                    SecretKey keyAES = Crypto.generateKey();
                                    byte[] keyAESBase64 = Base64.encode(Crypto.encrypt(pk, keyAES.getEncoded()), 0);
                                    String encryptedPk = new String(keyAESBase64);
                                    Crypto.storeSecretKey(keyAES, this.actualUser + "_to_" + contact.getUsername());
                                    String message = this.actualUser + "[|]PONG[|]" + encryptedPk;

                                    SendMessageTask task = new SendMessageTask(contact.getUsername(), message, mContext);
                                    task.setOnTaskCompleted(new OnTaskCompleted() {
                                        @Override
                                        public void onTaskCompleted() {
                                            db.addMessage(new MessageModel(null, actualUser, contact.getUsername(), message, true), actualUser);
                                            refreshListMessage();
                                            enabledMessageBoxAndButtonSend();
                                            btnActionCrypto.setVisibility(View.INVISIBLE);
                                        }

                                        @Override
                                        public void onFailure(String message) {
                                            System.out.println(message);
                                        }
                                    });
                                    task.execute();

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            });
                        } else {
                            MessageModel lastPong = messagesPong.get(messagesPong.size() - 1);
                            PrivateKey privateKey = ((KeyStore.PrivateKeyEntry) keyStore.getEntry(this.actualUser, null)).getPrivateKey();
                            SecretKey secretKey = Crypto.stringEncryptedToSecretKey(lastPong.getMessage(), privateKey);
                            Crypto.storeSecretKey(secretKey, this.actualUser + "_to_" + contact.getUsername());
                            enabledMessageBoxAndButtonSend();
                            btnActionCrypto.setVisibility(View.INVISIBLE);
                        }
                    }
                } else {
                    btnActionCrypto.setVisibility(View.INVISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            this.setVisibiltyElements(View.INVISIBLE);
        }
    }

    private void setVisibiltyElements(int visibility) {
        btnActionCrypto.setVisibility(visibility);
        btnSend.setVisibility(visibility);
        txtName.setVisibility(visibility);
        listView.setVisibility(visibility);
        boxMessage.setVisibility(visibility);
    }

    /**
     * Enable box message and send button if key AES for this contact exist
     */
    private void enabledMessageBoxAndButtonSend() {
        try {
            KeyStore keyStore = Crypto.getKeyStore();
            int state = View.VISIBLE;
            // Hide EditText for message and btn send if key to encrypt message not exist
            if (!keyStore.containsAlias(this.actualUser + "_to_" + contact.getUsername())) {
                state = View.INVISIBLE;
            }
            this.boxMessage.setVisibility(state);
            this.btnSend.setVisibility(state);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Method to refresh list message
     */
    public void refreshListMessage() {
        ArrayList<MessageModel> listMessage = db.readMessageByAuthorAndReceiver(contact.getUsername(), actualUser);
        SecretKey secretKey = null;
        try {
            KeyStore keyStore = Crypto.getKeyStore();
            if (keyStore.containsAlias(this.actualUser + "_to_" + contact.getUsername())) {
                secretKey = ((KeyStore.SecretKeyEntry) keyStore.getEntry(this.actualUser + "_to_" + contact.getUsername(), null)).getSecretKey();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        final MessageAdapter adapter = new MessageAdapter(listMessage, secretKey);
        listView.setAdapter(adapter);
        if (adapter.getItemCount() > 0) {
            listView.smoothScrollToPosition(adapter.getItemCount() - 1);
        }
    }

    public void returnToListContact() {
        this.mCallback.returnToListContact();
    }

    public void sendMessage() {
        try {
            KeyStore keyStore = Crypto.getKeyStore();
            SecretKey keyAES = ((KeyStore.SecretKeyEntry) keyStore.getEntry(this.actualUser + "_to_" + contact.getUsername(), null)).getSecretKey();
            String contentMessageNotEncrypted = boxMessage.getText().toString();
            EncryptedDataAES encryptedDataAES = Crypto.encryptAES(keyAES, contentMessageNotEncrypted.getBytes("UTF-8"));
            String message = this.actualUser + "[|]MSG[|]" + new String(Base64.encode(encryptedDataAES.getData(), 0), "UTF-8")
                    + "[|]CIPHERIV[|]" + new String(Base64.encode(encryptedDataAES.getCipherIV(), 0), "UTF-8");
            SendMessageTask task = new SendMessageTask(contact.getUsername(), message, mContext);
            task.setOnTaskCompleted(new OnTaskCompleted() {
                @Override
                public void onTaskCompleted() {
                    db.addMessage(new MessageModel(null, actualUser, contact.getUsername(), message, true), actualUser);
                    boxMessage.setText("");
                    refreshListMessage();
                }

                @Override
                public void onFailure(String message) {
                    System.out.println(message);
                }
            });
            task.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setContentMessage(String message) {
        this.contentMessage = message;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            mContext.unregisterReceiver(broadcastReceiver);
        } catch (IllegalArgumentException e) {
            System.out.println("Receiver not registered: " + e.getMessage());
        }
    }
}