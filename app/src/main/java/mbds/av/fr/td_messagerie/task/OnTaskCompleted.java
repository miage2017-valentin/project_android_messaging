package mbds.av.fr.td_messagerie.task;

public interface OnTaskCompleted {
    void onTaskCompleted();

    void onFailure(String message);
}
