package mbds.av.fr.td_messagerie.task;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginTask extends AsyncTask<Void, Void, JSONObject> {

    private String username, password;
    private Exception exception;
    private Context context;
    private OnTaskCompleted listener;

    public LoginTask(String username, String password, Context context) {
        this.username = username;
        this.password = password;
        this.context = context;
    }

    public void setOnTaskCompleted(OnTaskCompleted listener) {
        this.listener = listener;
    }

    protected void onPreExecute() {
    }

    protected JSONObject doInBackground(Void... urls) {
        System.out.println("---- SEND ----");
        try {
            URL url = new URL("http://baobab.tokidev.fr/api/login");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("username", username);
                jsonObject.put("password", password);
                System.out.println(jsonObject.toString());
                byte[] outputInBytes = jsonObject.toString().getBytes("UTF-8");
                OutputStream os = urlConnection.getOutputStream();
                os.write(outputInBytes);
                os.close();
                System.out.println("Code response : " + urlConnection.getResponseCode());
                InputStream responseBody = urlConnection.getInputStream();
                InputStreamReader responseBodyReader =
                        new InputStreamReader(responseBody, "UTF-8");
                BufferedReader streamReaderBuffer = new BufferedReader(responseBodyReader);
                String inputStr;
                StringBuilder responseStrBuilder = new StringBuilder();
                while ((inputStr = streamReaderBuffer.readLine()) != null)
                    responseStrBuilder.append(inputStr);

                JSONObject objResponse = new JSONObject(responseStrBuilder.toString());
                return objResponse;
            } catch (Exception ex) {
                System.out.println("ERROR : " + ex.getMessage());
                return null;
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e("ERROR OPEN CONNECTION : ", e.getMessage(), e);
            return null;
        }
    }

    protected void onPostExecute(JSONObject response) {
        try {
            if (response == null) {
                Log.d("DEBUG API", "ERROR response");
                listener.onFailure("Erreur API");
            } else {
                Log.i("INFO", response.get("access_token").toString());
                Log.d("DEBUG API", response.get("access_token").toString());
                SharedPreferences sharedPreferences = context.getSharedPreferences("api", Context.MODE_PRIVATE);
                sharedPreferences.edit().putString("token", response.get("access_token").toString()).commit();
                sharedPreferences.edit().putString("actualUser", this.username).commit();
                listener.onTaskCompleted();
            }
        } catch (JSONException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}