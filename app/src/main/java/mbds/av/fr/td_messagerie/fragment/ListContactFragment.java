package mbds.av.fr.td_messagerie.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

import mbds.av.fr.td_messagerie.AddContactActivity;
import mbds.av.fr.td_messagerie.ICallable;
import mbds.av.fr.td_messagerie.R;
import mbds.av.fr.td_messagerie.Utils.ItemClickSupport;
import mbds.av.fr.td_messagerie.adapter.ContactAdapter;
import mbds.av.fr.td_messagerie.database.Database;
import mbds.av.fr.td_messagerie.model.ContactModel;

public class ListContactFragment extends Fragment {
    private ICallable mCallback;
    private RecyclerView listView;
    private FloatingActionButton btnAddContact;
    private FloatingActionButton btnLogout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_list_contact, container, false);
        listView = view.findViewById(R.id.listContact);
        this.btnAddContact = view.findViewById(R.id.btnAddContact);
        btnLogout = view.findViewById(R.id.btnLogout);
        btnAddContact.setOnClickListener(v -> showAddContactActivity());
        btnLogout.setOnClickListener(v -> logout());
        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(view.getContext());
        listView.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(listView.getContext(),
                mLayoutManager.getOrientation());
        listView.addItemDecoration(dividerItemDecoration);
        refreshListContact();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ICallable) {
            mCallback = (ICallable) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement iCallable");
        }
    }

    public void showAddContactActivity() {
        this.mCallback.openActivity(AddContactActivity.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.refreshListContact();
    }

    public void refreshListContact() {
        SharedPreferences pref = getContext().getSharedPreferences("api", Context.MODE_PRIVATE);
        String actualUser = pref.getString("actualUser", "");

        ArrayList<ContactModel> listContact = Database.getInstance(getContext()).readContacts(actualUser);

        final ContactAdapter adapter = new ContactAdapter(listContact, actualUser, getContext());
        listView.setAdapter(adapter);
        ItemClickSupport.addTo(listView, R.layout.fragment_list_contact)
                .setOnItemClickListener((recyclerView, position, v) -> {
                    // 1 - Get user from adapter
                    ContactModel contact = adapter.getContact(position);
                    // 2 - Show result in a Toast
                    Toast.makeText(getContext(), "You clicked on user : " + contact.getName(), Toast.LENGTH_SHORT).show();
                    mCallback.changeContact(contact);
                });
    }

    private void logout() {
        this.mCallback.logout();
    }
}
