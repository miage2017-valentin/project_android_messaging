package mbds.av.fr.td_messagerie.task;

import java.util.ArrayList;

public interface OnRetrieveListTask<T> {
    void onTaskCompleted(ArrayList<T> listContact);

    void onFailure(String message);
}
