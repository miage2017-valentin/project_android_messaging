package mbds.av.fr.td_messagerie.task;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class SendMessageTask extends AsyncTask<Void, Void, JSONObject> {

    private OnTaskCompleted listener;
    private SharedPreferences sharedPreferences;
    private String receiver, message;

    public SendMessageTask(String receiver, String message, Context context) {
        this.receiver = receiver;
        this.message = message;
        this.sharedPreferences = context.getSharedPreferences("api", Context.MODE_PRIVATE);
    }

    public void setOnTaskCompleted(OnTaskCompleted listener) {
        this.listener = listener;
    }

    protected JSONObject doInBackground(Void... urls) {

        System.out.println("---- SEND ----");
        try {
            URL url = new URL("http://baobab.tokidev.fr/api/sendMsg");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Authorization", "Bearer " + this.sharedPreferences.getString("token", ""));
                System.out.println("Token : " + this.sharedPreferences.getString("token", "No token"));
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("message", message);
                jsonObject.put("receiver", receiver);
                System.out.println(jsonObject.toString());
                byte[] outputInBytes = jsonObject.toString().getBytes("UTF-8");
                OutputStream os = urlConnection.getOutputStream();
                os.write(outputInBytes);
                os.close();
                System.out.println("Code response : " + urlConnection.getResponseCode());
                InputStream responseBody = urlConnection.getInputStream();
                InputStreamReader responseBodyReader =
                        new InputStreamReader(responseBody, "UTF-8");
                BufferedReader streamReaderBuffer = new BufferedReader(responseBodyReader);
                String inputStr;
                StringBuilder responseStrBuilder = new StringBuilder();
                while ((inputStr = streamReaderBuffer.readLine()) != null)
                    responseStrBuilder.append(inputStr);

                JSONObject objResponse = new JSONObject(responseStrBuilder.toString());
                return objResponse;
            } catch (Exception ex) {
                System.out.println("ERROR : " + ex.getMessage());
                return null;
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e("ERROR OPEN CONNECTION : ", e.getMessage(), e);
            return null;
        }
    }

    protected void onPostExecute(JSONObject response) {
        if (response == null) {
            Log.d("DEBUG API", "ERROR response");
            listener.onFailure("ERROR API");
        } else {
            System.out.println("DEBUG API : " + response.toString());
            listener.onTaskCompleted();
        }
    }
}
