package mbds.av.fr.td_messagerie.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import mbds.av.fr.td_messagerie.R;
import mbds.av.fr.td_messagerie.database.Database;
import mbds.av.fr.td_messagerie.model.ContactModel;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {
    private Database db;
    private ArrayList<ContactModel> dataSet;
    private String actualUser;

    public ContactAdapter(ArrayList<ContactModel> data, String actualUser ,Context context) {
        this.dataSet = data;
        this.actualUser = actualUser;
        db = Database.getInstance(context);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ContactModel c = dataSet.get(position);
        holder.txtFullName.setText(c.getLastName() + " " + c.getName());
        holder.txtUsername.setText(c.getUsername());

        holder.btnDeleteContact.setOnClickListener(v -> {
            if(db.deleteContact(actualUser, c.getUsername())){
                dataSet.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, dataSet.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_contact, parent, false);
        return new MyViewHolder(v);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtFullName;
        public TextView txtUsername;
        public ImageButton btnDeleteContact;

        public MyViewHolder(View v) {
            super(v);
            this.txtFullName = v.findViewById(R.id.labelFullName);
            this.txtUsername = v.findViewById(R.id.labelUsername);
            this.btnDeleteContact = v.findViewById(R.id.btnDeleteContact);
        }
    }

    public ContactModel getContact(int position) {
        return this.dataSet.get(position);
    }
}
