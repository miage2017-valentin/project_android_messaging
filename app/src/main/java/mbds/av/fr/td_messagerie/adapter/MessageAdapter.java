package mbds.av.fr.td_messagerie.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import javax.crypto.SecretKey;

import mbds.av.fr.td_messagerie.R;
import mbds.av.fr.td_messagerie.model.MessageModel;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {

    private ArrayList<MessageModel> dataSet;
    private SecretKey secretKey;

    public MessageAdapter(ArrayList<MessageModel> data, SecretKey secretKey) {
        this.dataSet = data;
        this.secretKey = secretKey;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MessageModel message = dataSet.get(position);
        holder.txtContent.setText(message.getDecodedMessage(secretKey, true));
        holder.txtDate.setText(message.getDateFormatted());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_message, parent, false);
        return new MyViewHolder(v);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtContent;
        public TextView txtDate;

        public MyViewHolder(View v) {
            super(v);
            this.txtContent = v.findViewById(R.id.txtContent);
            this.txtDate = v.findViewById(R.id.txtDate);
        }
    }

    public MessageModel getMessage(int position) {
        return this.dataSet.get(position);
    }

    public ArrayList<MessageModel> getMessages() {
        return this.dataSet;
    }
}
