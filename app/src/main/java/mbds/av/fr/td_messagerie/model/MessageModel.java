package mbds.av.fr.td_messagerie.model;

import android.util.Base64;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.SecretKey;

import mbds.av.fr.td_messagerie.Utils.Crypto;
import mbds.av.fr.td_messagerie.Utils.EncryptedDataAES;
import mbds.av.fr.td_messagerie.enums.ETypeMessage;

public class MessageModel {
    private Long id;
    private String receiver;
    private String author;
    private String content;
    private Date dateCreated;
    private boolean alreadyReturned;

    public MessageModel(Long id, String author, String receiver, String content, boolean alreadyReturned) {
        this.id = id;
        this.author = author;
        this.receiver = receiver;
        this.alreadyReturned = alreadyReturned;
        this.content = content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateFormatted() {
        SimpleDateFormat destFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return destFormat.format(dateCreated);
    }

    public String getAuthor() {
        return author;
    }

    public boolean isAlreadyReturned() {
        return alreadyReturned;
    }

    public String getReceiver() {
        return receiver;
    }

    public ETypeMessage getType() {
        Pattern patternType = Pattern.compile("\\[\\|\\](.*?)\\[\\|\\]");
        if (content != null) {
            Matcher matcherType = patternType.matcher(content);
            if (matcherType.find()) {
                String type = matcherType.group(1);
                return ETypeMessage.valueOf(type);
            } else {
                return null;
            }
        }
        return null;
    }

    public String getMessage() {
        content = content.replace("\n", "");
        Pattern patternContent = Pattern.compile("^.*\\[\\|\\](.*?)$");
        if (content.contains("CIPHERIV")) {
            patternContent = Pattern.compile("^.*\\[\\|\\](.*?)\\[\\|\\]CIPHERIV");
        }
        Matcher matcherContent = patternContent.matcher(content);
        if (matcherContent.find()) {
            String contentFound = matcherContent.group(1);
            return contentFound;
        } else {
            return null;
        }
    }

    public String getCipherIV() {
        content = content.replace("\n", "");
        Pattern patternContent = Pattern.compile("^.*\\[\\|\\](.*?)$");
        Matcher matcherContent = patternContent.matcher(content);
        if (matcherContent.find()) {
            String contentFound = matcherContent.group(1);
            return contentFound;
        } else {
            return null;
        }
    }

    public String getDecodedMessage(SecretKey secretKey, boolean withAuthor) {
        String decodedMsg = "";
        if (this.getType().equals(ETypeMessage.MSG) && secretKey != null) {
            try {
                byte[] msgEncrypted = Base64.decode(this.getMessage(), 0);
                byte[] cipherIV = Base64.decode(this.getCipherIV(), 0);
                byte[] msgDecrypted = Crypto.decryptAES(secretKey, new EncryptedDataAES(msgEncrypted, cipherIV));
                decodedMsg = (withAuthor)
                        ? this.getAuthor() + ": " + new String(msgDecrypted, "UTF-8")
                        : new String(msgDecrypted, "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (this.getType().equals(ETypeMessage.PING)) {
            decodedMsg = "Key request by " + this.getAuthor();
        } else if (this.getType().equals(ETypeMessage.PONG)) {
            decodedMsg = "Key sent by " + this.getAuthor();
        } else {
            decodedMsg = this.getContent();
        }
        return decodedMsg;
    }
}
