package mbds.av.fr.td_messagerie.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MessageHelper extends SQLiteOpenHelper {
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Database.MessageContract.FeedContact.TABLE_NAME + " (" +
                    Database.MessageContract.FeedContact._ID + " INTEGER PRIMARY KEY," +
                    Database.MessageContract.FeedContact.COLUMN_RECEIVER + " TEXT," +
                    Database.MessageContract.FeedContact.COLUMN_AUTHOR + " TEXT," +
                    Database.MessageContract.FeedContact.COLUMN_CONTENT + " TEXT," +
                    Database.MessageContract.FeedContact.COLUMN_CREATED_BY + " TEXT," +
                    Database.MessageContract.FeedContact.COLUMN_CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP"
                    + ")";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Database.MessageContract.FeedContact.TABLE_NAME;
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "Messagerie.db";

    public MessageHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}