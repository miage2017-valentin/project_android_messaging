package mbds.av.fr.td_messagerie.model;

public class ContactModel {
    private String username;
    private String lastName;
    private String name;
    private Long id;

    public ContactModel(Long id, String username, String name, String lastName) {
        this.id = id;
        this.username = username;
        this.lastName = lastName;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return this.lastName + " " + this.name;
    }
}
