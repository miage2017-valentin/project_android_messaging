package mbds.av.fr.td_messagerie.task;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import mbds.av.fr.td_messagerie.Utils.JsonUtil;

public class RegisterUserTask extends AsyncTask<Void, Void, String> {

    private OnTaskCompleted listener;
    private String username, password;

    public RegisterUserTask(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void setOnTaskCompleted(OnTaskCompleted listener) {
        this.listener = listener;
    }

    protected void onPreExecute() {
    }

    protected String doInBackground(Void... urls) {

        System.out.println("---- SEND ----");
        try {
            URL url = new URL("http://baobab.tokidev.fr/api/createUser");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("username", username);
                jsonObject.put("password", password);
                System.out.println(jsonObject.toString());
                byte[] outputInBytes = jsonObject.toString().getBytes("UTF-8");
                OutputStream os = urlConnection.getOutputStream();
                os.write(outputInBytes);
                os.close();
                int statusCode = urlConnection.getResponseCode();
                System.out.println("Code response : " + statusCode);
                InputStream responseBody;
                if (statusCode >= 200 && statusCode < 400) {
                    // Create an InputStream in order to extract the response object
                    responseBody = urlConnection.getInputStream();
                } else {
                    responseBody = urlConnection.getErrorStream();
                }
                InputStreamReader responseBodyReader =
                        new InputStreamReader(responseBody, "UTF-8");
                BufferedReader streamReaderBuffer = new BufferedReader(responseBodyReader);
                String inputStr;
                StringBuilder responseStrBuilder = new StringBuilder();
                while ((inputStr = streamReaderBuffer.readLine()) != null)
                    responseStrBuilder.append(inputStr);

                return responseStrBuilder.toString();
            } catch (Exception ex) {
                System.out.println("ERROR : " + ex.getMessage());
                return null;
            } finally {
                urlConnection.disconnect();
            }
        } catch (Exception e) {
            Log.e("ERROR OPEN CONNECTION : ", e.getMessage(), e);
            return null;
        }
    }

    protected void onPostExecute(String response) {
        if (response == null) {
            Log.d("DEBUG API", "ERROR response");
            listener.onFailure(response);
        } else {
            if (JsonUtil.isJSONValid(response)) {
                System.out.println("DEBUG API : " + response.toString());
                listener.onTaskCompleted();
            } else {
                listener.onFailure(response);
            }
        }
    }
}
