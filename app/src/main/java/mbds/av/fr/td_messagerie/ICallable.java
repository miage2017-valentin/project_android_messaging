package mbds.av.fr.td_messagerie;

import mbds.av.fr.td_messagerie.model.ContactModel;

public interface ICallable {
    void changeContact(ContactModel contactModel);
    void returnToListContact();
    void openActivity(Class activity);
    void logout();
}
