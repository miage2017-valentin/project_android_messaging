package mbds.av.fr.td_messagerie.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import mbds.av.fr.td_messagerie.model.ContactModel;
import mbds.av.fr.td_messagerie.model.MessageModel;

public class Database {
    private static Database sSoleInstance;
    private MessageHelper messageHelper;
    private ContactHelper contactHelper;

    private Database(Context context) {
        messageHelper = new MessageHelper(context);
        contactHelper = new ContactHelper(context);
        if (sSoleInstance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public static Database getInstance(Context context) {
        if (sSoleInstance == null) {
            sSoleInstance = new Database(context);
        }

        return sSoleInstance;
    }

    public final class MessageContract {
        private MessageContract() {
        }

        public class FeedContact implements BaseColumns {
            public static final String TABLE_NAME = "Message";
            public static final String COLUMN_AUTHOR = "usernameAuthor";
            public static final String COLUMN_RECEIVER = "usernameReceiver";
            public static final String COLUMN_CONTENT = "content";
            public static final String COLUMN_CREATED_AT = "dateCreated";
            public static final String COLUMN_CREATED_BY = "createdBy";
        }
    }

    public final class ContactContract {
        private ContactContract() {
        }

        public class FeedContact implements BaseColumns {
            public static final String TABLE_NAME = "Contact";
            public static final String COLUMN_USERNAME = "username";
            public static final String COLUMN_LAST_NAME = "lastName";
            public static final String COLUMN_NAME = "name";
            public static final String COLUMN_CREATED_BY = "createdBy";
        }
    }

    public void addMessage(MessageModel messageModel, String actualUser) {
        // Gets the data repository in write mode
        SQLiteDatabase db = messageHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(MessageContract.FeedContact.COLUMN_CONTENT, messageModel.getContent());
        values.put(MessageContract.FeedContact.COLUMN_AUTHOR, messageModel.getAuthor());
        values.put(MessageContract.FeedContact.COLUMN_RECEIVER, messageModel.getReceiver());
        values.put(MessageContract.FeedContact.COLUMN_CREATED_BY, actualUser);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(MessageContract.FeedContact.TABLE_NAME, null, values);
    }

    public ArrayList<MessageModel> readMessageByAuthorAndReceiver(String usernameAuthor, String actualUser) {
        SQLiteDatabase db = messageHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                MessageContract.FeedContact.COLUMN_CONTENT,
                MessageContract.FeedContact.COLUMN_RECEIVER,
                MessageContract.FeedContact.COLUMN_AUTHOR,
                MessageContract.FeedContact.COLUMN_CREATED_AT
        };


        String selection = "(" + MessageContract.FeedContact.COLUMN_AUTHOR + " = '" + usernameAuthor + "'" +
                " OR (" + MessageContract.FeedContact.COLUMN_AUTHOR + " = '" + actualUser + "'" +
                " AND " + MessageContract.FeedContact.COLUMN_RECEIVER + " = '" + usernameAuthor + "'))" +
                " AND " + MessageContract.FeedContact.COLUMN_CREATED_BY + " = '" + actualUser + "'";
        String[] selectionArgs = null;

        String sortOrder =
                MessageContract.FeedContact.COLUMN_CREATED_AT + " ASC";

        Cursor cursor = db.query(
                MessageContract.FeedContact.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,
                sortOrder               // The sort order
        );

        ArrayList messages = new ArrayList<MessageModel>();
        while (cursor.moveToNext()) {
            long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(MessageContract.FeedContact._ID));
            String receiver = cursor.getString(cursor.getColumnIndex(MessageContract.FeedContact.COLUMN_RECEIVER));
            String author = cursor.getString(cursor.getColumnIndex(MessageContract.FeedContact.COLUMN_AUTHOR));
            String content = cursor.getString(cursor.getColumnIndex(MessageContract.FeedContact.COLUMN_CONTENT));
            String strCreatedAt = cursor.getString(cursor.getColumnIndex(MessageContract.FeedContact.COLUMN_CREATED_AT));
            MessageModel message = new MessageModel(itemId, author, receiver, content, true);
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            try {
                message.setDateCreated(dateFormat.parse(strCreatedAt));
                messages.add(message);
            } catch (ParseException e) {
                System.out.println(e.getMessage());
            }
        }
        cursor.close();

        return messages;
    }


    public void addContact(String username, String lastName, String name, String actualUser) {
        // Gets the data repository in write mode
        SQLiteDatabase db = contactHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(ContactContract.FeedContact.COLUMN_USERNAME, username);
        values.put(ContactContract.FeedContact.COLUMN_NAME, name);
        values.put(ContactContract.FeedContact.COLUMN_LAST_NAME, lastName);
        values.put(ContactContract.FeedContact.COLUMN_CREATED_BY, actualUser);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(ContactContract.FeedContact.TABLE_NAME, null, values);
    }

    public ArrayList<ContactModel> readContacts(String actualUser) {
        SQLiteDatabase db = contactHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                ContactContract.FeedContact.COLUMN_USERNAME,
                ContactContract.FeedContact.COLUMN_NAME,
                ContactContract.FeedContact.COLUMN_LAST_NAME
        };


        String selection = ContactContract.FeedContact.COLUMN_CREATED_BY + " = '" + actualUser + "'";
        String[] selectionArgs = null;

        String sortOrder =
                ContactContract.FeedContact.COLUMN_USERNAME + " ASC";

        Cursor cursor = db.query(
                ContactContract.FeedContact.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,
                sortOrder               // The sort order
        );

        ArrayList contacts = new ArrayList<ContactModel>();
        while (cursor.moveToNext()) {
            long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(ContactContract.FeedContact._ID));
            String username = cursor.getString(cursor.getColumnIndex(ContactContract.FeedContact.COLUMN_USERNAME));
            String lastName = cursor.getString(cursor.getColumnIndex(ContactContract.FeedContact.COLUMN_LAST_NAME));
            String name = cursor.getString(cursor.getColumnIndex(ContactContract.FeedContact.COLUMN_NAME));
            ContactModel contact = new ContactModel(itemId, username, lastName, name);
            contacts.add(contact);
        }
        cursor.close();

        return contacts;
    }

    public ContactModel readContactById(String actualUser, Long id) {
        SQLiteDatabase db = contactHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                ContactContract.FeedContact.COLUMN_USERNAME,
                ContactContract.FeedContact.COLUMN_NAME,
                ContactContract.FeedContact.COLUMN_LAST_NAME
        };


        String selection = ContactContract.FeedContact.COLUMN_CREATED_BY + " = '" + actualUser + "' " +
                "AND " + BaseColumns._ID + "= " + id;
        String[] selectionArgs = null;

        String sortOrder = null;

        Cursor cursor = db.query(
                ContactContract.FeedContact.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,
                sortOrder               // The sort order
        );
        cursor.moveToFirst();
        long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(ContactContract.FeedContact._ID));
        String username = cursor.getString(cursor.getColumnIndex(ContactContract.FeedContact.COLUMN_USERNAME));
        String lastName = cursor.getString(cursor.getColumnIndex(ContactContract.FeedContact.COLUMN_LAST_NAME));
        String name = cursor.getString(cursor.getColumnIndex(ContactContract.FeedContact.COLUMN_NAME));
        ContactModel contact = new ContactModel(itemId, username, lastName, name);

        cursor.close();

        return contact;
    }

    public boolean deleteContact(String actualUser, String userDeleted) {
        SQLiteDatabase db = contactHelper.getWritableDatabase();
        return db.delete(ContactContract.FeedContact.TABLE_NAME,
                ContactContract.FeedContact.COLUMN_USERNAME + "='" + userDeleted + "' "
                        + "AND " + ContactContract.FeedContact.COLUMN_CREATED_BY + "='" + actualUser + "'"
                , null) > 0;
    }
}
