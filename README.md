# Informations importantes
Android: 7.0 - API 24
Formes des messages:
```
**Alice[|]PING[|]clef publique d’Alice**
```
```
**Bob[|]PONG[|]clefAESchiffréeAvecKpA**
```
```
**Alice[|]MSG[|]messageChiffréeAvecAES[|]CIPHERIV[|]cipherIVGénéréeLorsDuChiffrement**
```

# Scénario type
 - Création d'un compte X
 - Création d'un compte Y
 - Connexion sur le compte X
 - Création d'un contact avec comme "username" : Y
 - Clique sur ce contact pour afficher la liste des messages
 - Demande de clé de chiffrement en cliquant sur le bouton "Ask key"
 - Déconnexion
 - Connexion sur le compte Y
 - Création d'un contact avec comme "username" : X
 - Clique sur ce contact pour afficher la liste des messages
 - Envoi de la clé de chiffrement en cliquant sur le bouton "Send key"
 - Les deux contacts peuvent maintenant s'envoyer des messages
 
# Fonctionnalités développées 
 - Login  (username, password) (connexion)
 - Register (username, password, …) (créer un utilisateur)
 - ContactList(usernames) (liste des contactes avec nombre de messages à lire)
 - Messagerie(conversation avec une personne, champ texte et bouton pour envoyer)
 - AjouterContact(username,...)
 - Service récupérant les messages pour chaque contact en base de données
 - Notification lors de la réception d'un nouveau message
 - Intent SEND: permet de partager du texte d'une autre application à un contact
 - Suppression d'un contact
